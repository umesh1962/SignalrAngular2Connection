import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ConnectionResolver} from "../../app/app.servce";
import { SignalRConnection } from 'ng2-signalr';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
 private _connection: SignalRConnection;
  id:any;
  constructor(public navCtrl: NavController,private _s:ConnectionResolver,) {
      this.id=this._s.resolve();
  }

}
